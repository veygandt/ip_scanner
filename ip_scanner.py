import sys
import socket
import asyncio
import ipaddress
import time
from random import randrange
from itertools import islice
from pathlib import Path


G_SUCCESS_COUNTER = 0
G_FAIL_COUNTER = 0
G_NO_RESULT_COUNTER = 0
G_IP_COUNTER = 0

sem = asyncio.Semaphore(500)
out_file_lock = asyncio.Lock()
counter_lock = asyncio.Lock()


def print_manifest():
    print(
        """ 
    This program check list of ip addresses for availabilities.\n
    If you run it without parameters, you can read this help.
    Try 'ip_scanner --help' for more information.
    Usage: ip_scanner [OPTION]
    Options:
    <filename>                                     scan ip addresses from file. Example: "python ip_scanner.py ip_list.txt"
    <filename> <out_filename>                      scan ip addresses from file and write result to out_filename. Example:
                                                    "python ip_scanner.py ip_list.txt"

    -s, --subnet <subnet>                          scan subnet. Example: "python ip_scanner.py -s 80.80.80.0/27"
    -t                                             test run - scan subnet 86.250.0.0/16. Example:  "python ip_scanner.py -t" 
    -i, --ip <ip_address>                          scan one ip address. Example: "python ip_scanner.py 86.250.104.16"
    -g,  --gen_ip_file <number of ip> <filename>   generate file of random ip. Example: "python ip_scanner.py -g 50000 ip_list.txt"
    """)


def scan_one_ip(ip):
    sock = socket.socket()
    sock.settimeout(5)
    try:
        sock.connect((ip, 80))
        print(F"{ip}   - OK")
    except socket.error:
        print(F"{ip} - error")
    finally:
        sock.close()


async def scanner(ip, port, out_file_h, loop=None):
    ip = ip.strip()
    async with sem:
        connect = asyncio.open_connection(ip, port, loop=loop)
        global G_IP_COUNTER
        global G_SUCCESS_COUNTER
        global G_FAIL_COUNTER
        global G_NO_RESULT_COUNTER
        async with counter_lock:
            G_IP_COUNTER += 1

        try:
            reader, writer = await asyncio.wait_for(connect, timeout=0.3)
            G_SUCCESS_COUNTER += 1
            if out_file_h:
                async with out_file_lock:
                    out_file_h.write(F"{ip} OK\n")

        except asyncio.TimeoutError:
            async with counter_lock:
                G_NO_RESULT_COUNTER += 1
            if out_file_h:
                async with out_file_lock:
                    out_file_h.write(F"{ip} NO_RESULT\n")
        except Exception as exc:
            async with counter_lock:
                G_FAIL_COUNTER += 1
            if out_file_h:
                async with out_file_lock:
                    out_file_h.write(F"{ip} FAIL\n")


def scan_subnet_ip(subnet):
    try:
        start_time = time.time()
        subnet_scan = ipaddress.ip_network(subnet)
        tasks2 = [scanner(str(ip), 80) for ip in subnet_scan]
        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.wait(tasks2))
        print_result()
        print(F'Work time: {(time.time()-start_time)} sec ')
    except ValueError as e:
        print(e)
        sys.exit(2)
    except Exception as e:
        print(F"Error in scanning subnet {e} ")
        sys.exit(2)


def ip_from_file_gen(filename):
    with open(filename, 'r') as file:
        for line in file:
            yield line


def scan_ip_from_file(filename, out_file):
    ip_list = ip_from_file_gen(filename)
    out_file_h = None
    try:
        if out_file:
            out_file_h = open(out_file, 'w')
    except Exception as e:
        print(e)
        sys.exit(2)
    try:
        start_time = time.time()
        tasks = [scanner(str(ip), 80, out_file_h) for ip in ip_list]
        loop = asyncio.get_event_loop()
        loop.run_until_complete(asyncio.wait(tasks))
        print_result()
        #print(F'Work time: {(time.time() - start_time)} sec ')
        if out_file_h:
            out_file_h.close()
    except ValueError as e:
        print(e)
        if out_file_h:
            out_file_h.close()
        sys.exit(2)
    except Exception as e:
        if out_file_h:
            out_file_h.close()
        print(F"Error in scanning ip list from file {e} ")
        sys.exit(2)


def ips():
    while True:
        a = randrange(256)
        b = randrange(256)
        c = randrange(256)
        d = randrange(256)
        yield f'{a}.{b}.{c}.{d}'


def write_out(filename, lines):
    with Path(filename).open('a', encoding='utf8') as fp:
        for line in lines:
            fp.write(f'{line}\n')


def generate_file_ip(number, filename):
    try:
        write_out(filename, islice(ips(), int(number)))
        print(F'File {filename} with {number} ip addresses has created!')

    except ValueError as e:
        print(e)
        sys.exit(2)
    except Exception as e:
        print(F"Error generating the file {e} ")
        sys.exit(2)


def print_result():
    #print(F"IP checked:        {G_IP_COUNTER}")
    print(F"Available:      {G_SUCCESS_COUNTER}")
    print(F"Not Available:  {G_FAIL_COUNTER}")
    print(F"NO result:      {G_NO_RESULT_COUNTER}")


def main():
    if len(sys.argv) == 1:
        print_manifest()

    elif len(sys.argv) > 4:
        print("Wrong number of arguments")
        print_manifest()
        return 1
    else:
        if (sys.argv[1] == '-s' or sys.argv[1] == '--subnet') and len(sys.argv) == 3:
            scan_subnet_ip(sys.argv[2])

        elif (sys.argv[1] == '-i' or sys.argv[1] == '--ip') and len(sys.argv) == 3:
            scan_one_ip(sys.argv[2])

        elif (sys.argv[1] == '-t' or sys.argv[1] == '--test') and len(sys.argv) == 2:
            scan_subnet_ip("86.250.0.0/16")

        elif (sys.argv[1] == '-g' or sys.argv[1] == '--gen_ip_file') and len(sys.argv) == 4:
            generate_file_ip(sys.argv[2], sys.argv[3])   # number of ip, filename

        elif sys.argv[1] == '--help':
            print_manifest()

        elif (len(sys.argv) == 2 or len(sys.argv) == 3) and sys.argv[1] != '-t':  # filename
            out_file = None
            if len(sys.argv) == 3:
                out_file = sys.argv[2]
            scan_ip_from_file(sys.argv[1], out_file)
        else:
            print("Wrong argument")
            print_manifest()
            sys.exit(2)


if __name__ == "__main__":
    main()
